//
//  ViewController.h
//  2048C
//
//  Created by Ciaran Slattery on 2/2/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


//https://www.youtube.com/watch?v=pjuqOM4LMK4
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * arrayofTiles;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * row0;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * row0B;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * row1;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * row1B;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * row2;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * row2B;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * row3;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * row3B;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * col0;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * col0B;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * col1;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * col1B;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * col2;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * col2B;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * col3;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray * col3B;

@property (nonatomic, strong) IBOutlet
    UILabel *t00, *t01, *t02, *t03, *t10, *t11, *t12, *t13, *t20, *t21, *t22, *t23, *t30, *t31, *t32, *t33;


@end

