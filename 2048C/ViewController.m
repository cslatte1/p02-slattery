//
//  ViewController.m
//  2048C
//
//  Created by Ciaran Slattery on 2/2/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize t00, t01, t02, t03, t10, t11, t12, t13, t20, t21, t22, t23, t30, t31, t32, t33;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)startPlay:(id)sender
{
    [t00 setText:@"4"];
    [t01 setText:@"2"];
    [t02 setText:@"2"];
    [t03 setText:@"8"];
    
    [t10 setText:@""];
    [t11 setText:@""];
    [t12 setText:@""];
    [t13 setText:@""];
    
    [t20 setText:@""];
    [t21 setText:@""];
    [t22 setText:@""];
    [t23 setText:@""];
    
    [t30 setText:@"8"];
    [t31 setText:@"2"];
    [t32 setText:@"2"];
    [t33 setText:@"4"];
}

-(IBAction)moveDown:(id)sender
{
    NSMutableArray *tileIntArray0 = [NSMutableArray array];
    int itr0 = 0;
    
    for(UILabel *tile in self.col0B){
        if([self isEmpty:tile]){
            itr0 = 10;
            [tileIntArray0 addObject:[NSNumber numberWithInteger:itr0]];
        }else{
            itr0 = tile.text.intValue;
            [tileIntArray0 addObject:[NSNumber numberWithInteger:itr0]];
        }
        NSLog(@"Int value: %d", itr0);
    }
    NSLog(@"%@", tileIntArray0);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray0[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray0[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray0[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray0[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray0[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray0[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray0[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray0[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray0[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray0[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray0);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray0 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray0[j] = tileIntArray0[j+1];
            }
            
            [tileIntArray0 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray0);
    
    int count0 = 0;
    
    for(UILabel *tile in self.col0B){
        NSLog(@"Tile %d = %@", count0, tile.text);
        NSLog(@"tIA index %d = %@", count0, [tileIntArray0 objectAtIndex:count0]);
        
        if([[tileIntArray0 objectAtIndex:count0] integerValue] == 2){
            NSLog(@"TILE %d == 2", count0);
            [tile setText:@"2"];
        }
        else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 4){
            NSLog(@"TILE %d == 4", count0);
            [tile setText:@"4"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 8){
            NSLog(@"TILE %d == 8", count0);
            [tile setText:@"8"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 16){
            NSLog(@"TILE %d == 16", count0);
            [tile setText:@"16"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 32){
            NSLog(@"TILE %d == 32", count0);
            [tile setText:@"32"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 64){
            NSLog(@"TILE %d == 64", count0);
            [tile setText:@"64"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 128){
            NSLog(@"TILE %d == 128", count0);
            [tile setText:@"128"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 256){
            NSLog(@"TILE %d == 256", count0);
            [tile setText:@"256"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 512){
            NSLog(@"TILE %d == 512", count0);
            [tile setText:@"512"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count0);
            [tile setText:@"1024"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count0);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count0++;
        
        
    }
    
    
    NSMutableArray *tileIntArray1 = [NSMutableArray array];
    int itr1 = 0;
    
    for(UILabel *tile in self.col1B){
        if([self isEmpty:tile]){
            itr1 = 10;
            [tileIntArray1 addObject:[NSNumber numberWithInteger:itr1]];
        }else{
            itr1 = tile.text.intValue;
            [tileIntArray1 addObject:[NSNumber numberWithInteger:itr1]];
        }
        NSLog(@"Int value: %d", itr1);
    }
    NSLog(@"%@", tileIntArray1);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray1[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
        }else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray1[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray1[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray1[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray1[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray1[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray1[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray1[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray1[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray1[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray1);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray1 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray1[j] = tileIntArray1[j+1];
            }
            
            [tileIntArray1 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray1);
    
    int count1 = 0;
    
    for(UILabel *tile in self.col1B){
        NSLog(@"Tile %d = %@", count1, tile.text);
        NSLog(@"tIA index %d = %@", count1, [tileIntArray1 objectAtIndex:count1]);
        
        if([[tileIntArray1 objectAtIndex:count1] integerValue] == 2){
            NSLog(@"TILE %d == 2", count1);
            [tile setText:@"2"];
        }
        else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 4){
            NSLog(@"TILE %d == 4", count1);
            [tile setText:@"4"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 8){
            NSLog(@"TILE %d == 8", count1);
            [tile setText:@"8"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 16){
            NSLog(@"TILE %d == 16", count1);
            [tile setText:@"16"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 32){
            NSLog(@"TILE %d == 32", count1);
            [tile setText:@"32"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 64){
            NSLog(@"TILE %d == 64", count1);
            [tile setText:@"64"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 128){
            NSLog(@"TILE %d == 128", count1);
            [tile setText:@"128"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 256){
            NSLog(@"TILE %d == 256", count1);
            [tile setText:@"256"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 512){
            NSLog(@"TILE %d == 512", count1);
            [tile setText:@"512"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count1);
            [tile setText:@"1024"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count1);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count1++;
        
        
    }
    
    NSMutableArray *tileIntArray2 = [NSMutableArray array];
    int itr2 = 0;
    
    for(UILabel *tile in self.col2B){
        if([self isEmpty:tile]){
            itr2 = 10;
            [tileIntArray2 addObject:[NSNumber numberWithInteger:itr2]];
        }else{
            itr2 = tile.text.intValue;
            [tileIntArray2 addObject:[NSNumber numberWithInteger:itr2]];
        }
        NSLog(@"Int value: %d", itr2);
    }
    NSLog(@"%@", tileIntArray2);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray2[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
        }else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray2[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray2[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray2[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray2[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray2[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray2[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray2[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray2[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray2[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray2);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray2 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray2[j] = tileIntArray2[j+1];
            }
            
            [tileIntArray2 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray2);
    
    int count2 = 0;
    
    for(UILabel *tile in self.col2B){
        NSLog(@"Tile %d = %@", count2, tile.text);
        NSLog(@"tIA index %d = %@", count2, [tileIntArray2 objectAtIndex:count2]);
        
        if([[tileIntArray2 objectAtIndex:count2] integerValue] == 2){
            NSLog(@"TILE %d == 2", count2);
            [tile setText:@"2"];
        }
        else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 4){
            NSLog(@"TILE %d == 4", count2);
            [tile setText:@"4"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 8){
            NSLog(@"TILE %d == 8", count2);
            [tile setText:@"8"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 16){
            NSLog(@"TILE %d == 16", count2);
            [tile setText:@"16"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 32){
            NSLog(@"TILE %d == 32", count2);
            [tile setText:@"32"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 64){
            NSLog(@"TILE %d == 64", count2);
            [tile setText:@"64"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 128){
            NSLog(@"TILE %d == 128", count2);
            [tile setText:@"128"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 256){
            NSLog(@"TILE %d == 256", count2);
            [tile setText:@"256"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 512){
            NSLog(@"TILE %d == 512", count2);
            [tile setText:@"512"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count2);
            [tile setText:@"1024"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count2);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count2++;
        
        
    }
    
    
    NSMutableArray *tileIntArray3 = [NSMutableArray array];
    int itr3 = 0;
    
    for(UILabel *tile in self.col3B){
        if([self isEmpty:tile]){
            itr3 = 10;
            [tileIntArray3 addObject:[NSNumber numberWithInteger:itr3]];
        }else{
            itr3 = tile.text.intValue;
            [tileIntArray3 addObject:[NSNumber numberWithInteger:itr3]];
        }
        NSLog(@"Int value: %d", itr3);
    }
    NSLog(@"%@", tileIntArray3);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray3[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
        }else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray3[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray3[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray3[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray3[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray3[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray3[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray3[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray3[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray3[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray3);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray3 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray3[j] = tileIntArray3[j+1];
            }
            
            [tileIntArray3 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray3);
    
    int count3 = 0;
    
    for(UILabel *tile in self.col3B){
        NSLog(@"Tile %d = %@", count3, tile.text);
        NSLog(@"tIA index %d = %@", count3, [tileIntArray3 objectAtIndex:count3]);
        
        if([[tileIntArray3 objectAtIndex:count3] integerValue] == 2){
            NSLog(@"TILE %d == 2", count3);
            [tile setText:@"2"];
        }
        else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 4){
            NSLog(@"TILE %d == 4", count3);
            [tile setText:@"4"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 8){
            NSLog(@"TILE %d == 8", count3);
            [tile setText:@"8"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 16){
            NSLog(@"TILE %d == 16", count3);
            [tile setText:@"16"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 32){
            NSLog(@"TILE %d == 32", count3);
            [tile setText:@"32"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 64){
            NSLog(@"TILE %d == 64", count3);
            [tile setText:@"64"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 128){
            NSLog(@"TILE %d == 128", count3);
            [tile setText:@"128"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 256){
            NSLog(@"TILE %d == 256", count3);
            [tile setText:@"256"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 512){
            NSLog(@"TILE %d == 512", count3);
            [tile setText:@"512"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count3);
            [tile setText:@"1024"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count3);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count3++;
        
        
    }
    
    //Assigns the next empty tile with a value of 2 to keep the game going.
    for(UILabel *tile in self.arrayofTiles){
        if([self isEmpty:tile]){
            [tile setText:@"2"];
            break;
        }
    }
}

-(IBAction)moveUp:(id)sender
{
    NSMutableArray *tileIntArray0 = [NSMutableArray array];
    int itr0 = 0;
    
    for(UILabel *tile in self.col0){
        if([self isEmpty:tile]){
            itr0 = 10;
            [tileIntArray0 addObject:[NSNumber numberWithInteger:itr0]];
        }else{
            itr0 = tile.text.intValue;
            [tileIntArray0 addObject:[NSNumber numberWithInteger:itr0]];
        }
        NSLog(@"Int value: %d", itr0);
    }
    NSLog(@"%@", tileIntArray0);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray0[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray0[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray0[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray0[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray0[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray0[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray0[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray0[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray0[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray0[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray0);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray0 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray0[j] = tileIntArray0[j+1];
            }
            
            [tileIntArray0 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray0);
    
    int count0 = 0;
    
    for(UILabel *tile in self.col0){
        NSLog(@"Tile %d = %@", count0, tile.text);
        NSLog(@"tIA index %d = %@", count0, [tileIntArray0 objectAtIndex:count0]);
        
        if([[tileIntArray0 objectAtIndex:count0] integerValue] == 2){
            NSLog(@"TILE %d == 2", count0);
            [tile setText:@"2"];
        }
        else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 4){
            NSLog(@"TILE %d == 4", count0);
            [tile setText:@"4"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 8){
            NSLog(@"TILE %d == 8", count0);
            [tile setText:@"8"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 16){
            NSLog(@"TILE %d == 16", count0);
            [tile setText:@"16"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 32){
            NSLog(@"TILE %d == 32", count0);
            [tile setText:@"32"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 64){
            NSLog(@"TILE %d == 64", count0);
            [tile setText:@"64"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 128){
            NSLog(@"TILE %d == 128", count0);
            [tile setText:@"128"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 256){
            NSLog(@"TILE %d == 256", count0);
            [tile setText:@"256"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 512){
            NSLog(@"TILE %d == 512", count0);
            [tile setText:@"512"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count0);
            [tile setText:@"1024"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count0);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count0++;
        
        
    }
    
    
    NSMutableArray *tileIntArray1 = [NSMutableArray array];
    int itr1 = 0;
    
    for(UILabel *tile in self.col1){
        if([self isEmpty:tile]){
            itr1 = 10;
            [tileIntArray1 addObject:[NSNumber numberWithInteger:itr1]];
        }else{
            itr1 = tile.text.intValue;
            [tileIntArray1 addObject:[NSNumber numberWithInteger:itr1]];
        }
        NSLog(@"Int value: %d", itr1);
    }
    NSLog(@"%@", tileIntArray1);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray1[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
        }else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray1[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray1[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray1[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray1[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray1[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray1[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray1[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray1[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray1[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray1);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray1 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray1[j] = tileIntArray1[j+1];
            }
            
            [tileIntArray1 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray1);
    
    int count1 = 0;
    
    for(UILabel *tile in self.col1){
        NSLog(@"Tile %d = %@", count1, tile.text);
        NSLog(@"tIA index %d = %@", count1, [tileIntArray1 objectAtIndex:count1]);
        
        if([[tileIntArray1 objectAtIndex:count1] integerValue] == 2){
            NSLog(@"TILE %d == 2", count1);
            [tile setText:@"2"];
        }
        else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 4){
            NSLog(@"TILE %d == 4", count1);
            [tile setText:@"4"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 8){
            NSLog(@"TILE %d == 8", count1);
            [tile setText:@"8"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 16){
            NSLog(@"TILE %d == 16", count1);
            [tile setText:@"16"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 32){
            NSLog(@"TILE %d == 32", count1);
            [tile setText:@"32"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 64){
            NSLog(@"TILE %d == 64", count1);
            [tile setText:@"64"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 128){
            NSLog(@"TILE %d == 128", count1);
            [tile setText:@"128"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 256){
            NSLog(@"TILE %d == 256", count1);
            [tile setText:@"256"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 512){
            NSLog(@"TILE %d == 512", count1);
            [tile setText:@"512"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count1);
            [tile setText:@"1024"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count1);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count1++;
        
        
    }
    
    NSMutableArray *tileIntArray2 = [NSMutableArray array];
    int itr2 = 0;
    
    for(UILabel *tile in self.col2){
        if([self isEmpty:tile]){
            itr2 = 10;
            [tileIntArray2 addObject:[NSNumber numberWithInteger:itr2]];
        }else{
            itr2 = tile.text.intValue;
            [tileIntArray2 addObject:[NSNumber numberWithInteger:itr2]];
        }
        NSLog(@"Int value: %d", itr2);
    }
    NSLog(@"%@", tileIntArray2);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray2[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
        }else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray2[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray2[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray2[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray2[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray2[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray2[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray2[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray2[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray2[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray2);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray2 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray2[j] = tileIntArray2[j+1];
            }
            
            [tileIntArray2 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray2);
    
    int count2 = 0;
    
    for(UILabel *tile in self.col2){
        NSLog(@"Tile %d = %@", count2, tile.text);
        NSLog(@"tIA index %d = %@", count2, [tileIntArray2 objectAtIndex:count2]);
        
        if([[tileIntArray2 objectAtIndex:count2] integerValue] == 2){
            NSLog(@"TILE %d == 2", count2);
            [tile setText:@"2"];
        }
        else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 4){
            NSLog(@"TILE %d == 4", count2);
            [tile setText:@"4"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 8){
            NSLog(@"TILE %d == 8", count2);
            [tile setText:@"8"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 16){
            NSLog(@"TILE %d == 16", count2);
            [tile setText:@"16"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 32){
            NSLog(@"TILE %d == 32", count2);
            [tile setText:@"32"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 64){
            NSLog(@"TILE %d == 64", count2);
            [tile setText:@"64"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 128){
            NSLog(@"TILE %d == 128", count2);
            [tile setText:@"128"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 256){
            NSLog(@"TILE %d == 256", count2);
            [tile setText:@"256"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 512){
            NSLog(@"TILE %d == 512", count2);
            [tile setText:@"512"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count2);
            [tile setText:@"1024"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count2);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count2++;
        
        
    }
    
    
    NSMutableArray *tileIntArray3 = [NSMutableArray array];
    int itr3 = 0;
    
    for(UILabel *tile in self.col3){
        if([self isEmpty:tile]){
            itr3 = 10;
            [tileIntArray3 addObject:[NSNumber numberWithInteger:itr3]];
        }else{
            itr3 = tile.text.intValue;
            [tileIntArray3 addObject:[NSNumber numberWithInteger:itr3]];
        }
        NSLog(@"Int value: %d", itr3);
    }
    NSLog(@"%@", tileIntArray3);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray3[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
        }else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray3[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray3[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray3[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray3[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray3[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray3[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray3[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray3[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray3[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray3);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray3 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray3[j] = tileIntArray3[j+1];
            }
            
            [tileIntArray3 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray3);
    
    int count3 = 0;
    
    for(UILabel *tile in self.col3){
        NSLog(@"Tile %d = %@", count3, tile.text);
        NSLog(@"tIA index %d = %@", count3, [tileIntArray3 objectAtIndex:count3]);
        
        if([[tileIntArray3 objectAtIndex:count3] integerValue] == 2){
            NSLog(@"TILE %d == 2", count3);
            [tile setText:@"2"];
        }
        else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 4){
            NSLog(@"TILE %d == 4", count3);
            [tile setText:@"4"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 8){
            NSLog(@"TILE %d == 8", count3);
            [tile setText:@"8"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 16){
            NSLog(@"TILE %d == 16", count3);
            [tile setText:@"16"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 32){
            NSLog(@"TILE %d == 32", count3);
            [tile setText:@"32"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 64){
            NSLog(@"TILE %d == 64", count3);
            [tile setText:@"64"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 128){
            NSLog(@"TILE %d == 128", count3);
            [tile setText:@"128"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 256){
            NSLog(@"TILE %d == 256", count3);
            [tile setText:@"256"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 512){
            NSLog(@"TILE %d == 512", count3);
            [tile setText:@"512"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count3);
            [tile setText:@"1024"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count3);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count3++;
        
        
    }
    
    //Assigns the next empty tile with a value of 2 to keep the game going.
    for(UILabel *tile in self.arrayofTiles){
        if([self isEmpty:tile]){
            [tile setText:@"2"];
            break;
        }
    }

}

-(IBAction)moveLeft:(id)sender
{
    NSMutableArray *tileIntArray0 = [NSMutableArray array];
    int itr0 = 0;
    
    for(UILabel *tile in self.row0){
        if([self isEmpty:tile]){
            itr0 = 10;
            [tileIntArray0 addObject:[NSNumber numberWithInteger:itr0]];
        }else{
            itr0 = tile.text.intValue;
            [tileIntArray0 addObject:[NSNumber numberWithInteger:itr0]];
        }
        NSLog(@"Int value: %d", itr0);
    }
    NSLog(@"%@", tileIntArray0);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray0[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray0[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray0[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray0[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray0[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray0[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray0[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray0[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray0[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray0[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray0);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray0 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray0[j] = tileIntArray0[j+1];
            }
            
            [tileIntArray0 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray0);
    
    int count0 = 0;
    
    for(UILabel *tile in self.row0){
        NSLog(@"Tile %d = %@", count0, tile.text);
        NSLog(@"tIA index %d = %@", count0, [tileIntArray0 objectAtIndex:count0]);
        
        if([[tileIntArray0 objectAtIndex:count0] integerValue] == 2){
            NSLog(@"TILE %d == 2", count0);
            [tile setText:@"2"];
        }
        else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 4){
            NSLog(@"TILE %d == 4", count0);
            [tile setText:@"4"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 8){
            NSLog(@"TILE %d == 8", count0);
            [tile setText:@"8"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 16){
            NSLog(@"TILE %d == 16", count0);
            [tile setText:@"16"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 32){
            NSLog(@"TILE %d == 32", count0);
            [tile setText:@"32"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 64){
            NSLog(@"TILE %d == 64", count0);
            [tile setText:@"64"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 128){
            NSLog(@"TILE %d == 128", count0);
            [tile setText:@"128"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 256){
            NSLog(@"TILE %d == 256", count0);
            [tile setText:@"256"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 512){
            NSLog(@"TILE %d == 512", count0);
            [tile setText:@"512"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count0);
            [tile setText:@"1024"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count0);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count0++;
        
        
    }

    
    NSMutableArray *tileIntArray1 = [NSMutableArray array];
    int itr1 = 0;
    
    for(UILabel *tile in self.row1){
        if([self isEmpty:tile]){
            itr1 = 10;
            [tileIntArray1 addObject:[NSNumber numberWithInteger:itr1]];
        }else{
            itr1 = tile.text.intValue;
            [tileIntArray1 addObject:[NSNumber numberWithInteger:itr1]];
        }
        NSLog(@"Int value: %d", itr1);
    }
    NSLog(@"%@", tileIntArray1);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray1[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
        }else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray1[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray1[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray1[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray1[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray1[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray1[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray1[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray1[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray1[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray1);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray1 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray1[j] = tileIntArray1[j+1];
            }
            
            [tileIntArray1 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray1);
    
    int count1 = 0;
    
    for(UILabel *tile in self.row1){
        NSLog(@"Tile %d = %@", count1, tile.text);
        NSLog(@"tIA index %d = %@", count1, [tileIntArray1 objectAtIndex:count1]);
        
        if([[tileIntArray1 objectAtIndex:count1] integerValue] == 2){
            NSLog(@"TILE %d == 2", count1);
            [tile setText:@"2"];
        }
        else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 4){
            NSLog(@"TILE %d == 4", count1);
            [tile setText:@"4"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 8){
            NSLog(@"TILE %d == 8", count1);
            [tile setText:@"8"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 16){
            NSLog(@"TILE %d == 16", count1);
            [tile setText:@"16"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 32){
            NSLog(@"TILE %d == 32", count1);
            [tile setText:@"32"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 64){
            NSLog(@"TILE %d == 64", count1);
            [tile setText:@"64"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 128){
            NSLog(@"TILE %d == 128", count1);
            [tile setText:@"128"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 256){
            NSLog(@"TILE %d == 256", count1);
            [tile setText:@"256"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 512){
            NSLog(@"TILE %d == 512", count1);
            [tile setText:@"512"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count1);
            [tile setText:@"1024"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count1);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count1++;
        
        
    }
    
    NSMutableArray *tileIntArray2 = [NSMutableArray array];
    int itr2 = 0;
    
    for(UILabel *tile in self.row2){
        if([self isEmpty:tile]){
            itr2 = 10;
            [tileIntArray2 addObject:[NSNumber numberWithInteger:itr2]];
        }else{
            itr2 = tile.text.intValue;
            [tileIntArray2 addObject:[NSNumber numberWithInteger:itr2]];
        }
        NSLog(@"Int value: %d", itr2);
    }
    NSLog(@"%@", tileIntArray2);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray2[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
        }else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray2[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray2[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray2[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray2[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray2[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray2[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray2[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray2[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray2[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray2);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray2 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray2[j] = tileIntArray2[j+1];
            }
            
            [tileIntArray2 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray2);
    
    int count2 = 0;
    
    for(UILabel *tile in self.row2){
        NSLog(@"Tile %d = %@", count2, tile.text);
        NSLog(@"tIA index %d = %@", count2, [tileIntArray2 objectAtIndex:count2]);
        
        if([[tileIntArray2 objectAtIndex:count2] integerValue] == 2){
            NSLog(@"TILE %d == 2", count2);
            [tile setText:@"2"];
        }
        else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 4){
            NSLog(@"TILE %d == 4", count2);
            [tile setText:@"4"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 8){
            NSLog(@"TILE %d == 8", count2);
            [tile setText:@"8"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 16){
            NSLog(@"TILE %d == 16", count2);
            [tile setText:@"16"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 32){
            NSLog(@"TILE %d == 32", count2);
            [tile setText:@"32"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 64){
            NSLog(@"TILE %d == 64", count2);
            [tile setText:@"64"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 128){
            NSLog(@"TILE %d == 128", count2);
            [tile setText:@"128"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 256){
            NSLog(@"TILE %d == 256", count2);
            [tile setText:@"256"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 512){
            NSLog(@"TILE %d == 512", count2);
            [tile setText:@"512"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count2);
            [tile setText:@"1024"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count2);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count2++;
        
        
    }
    
    
    NSMutableArray *tileIntArray3 = [NSMutableArray array];
    int itr3 = 0;
    
    for(UILabel *tile in self.row3){
        if([self isEmpty:tile]){
            itr3 = 10;
            [tileIntArray3 addObject:[NSNumber numberWithInteger:itr3]];
        }else{
            itr3 = tile.text.intValue;
            [tileIntArray3 addObject:[NSNumber numberWithInteger:itr3]];
        }
        NSLog(@"Int value: %d", itr3);
    }
    NSLog(@"%@", tileIntArray3);

    for(int i = 0; i < 3; i++){
        if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray3[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }

        }else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray3[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }


            //NSLog(@"Nah");
        }else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray3[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray3[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray3[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }

            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray3[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray3[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray3[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray3[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray3[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray3);

    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray3 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray3[j] = tileIntArray3[j+1];
            }
            
            [tileIntArray3 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray3);
    
    int count3 = 0;

    for(UILabel *tile in self.row3){
        NSLog(@"Tile %d = %@", count3, tile.text);
        NSLog(@"tIA index %d = %@", count3, [tileIntArray3 objectAtIndex:count3]);
        
        if([[tileIntArray3 objectAtIndex:count3] integerValue] == 2){
            NSLog(@"TILE %d == 2", count3);
            [tile setText:@"2"];
        }
        else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 4){
            NSLog(@"TILE %d == 4", count3);
            [tile setText:@"4"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 8){
            NSLog(@"TILE %d == 8", count3);
            [tile setText:@"8"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 16){
            NSLog(@"TILE %d == 16", count3);
            [tile setText:@"16"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 32){
            NSLog(@"TILE %d == 32", count3);
            [tile setText:@"32"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 64){
            NSLog(@"TILE %d == 64", count3);
            [tile setText:@"64"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 128){
            NSLog(@"TILE %d == 128", count3);
            [tile setText:@"128"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 256){
            NSLog(@"TILE %d == 256", count3);
            [tile setText:@"256"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 512){
            NSLog(@"TILE %d == 512", count3);
            [tile setText:@"512"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count3);
            [tile setText:@"1024"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count3);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }

        count3++;
        
        
    }
    
    //Assigns the next empty tile with a value of 2 to keep the game going.
    for(UILabel *tile in self.arrayofTiles){
        if([self isEmpty:tile]){
            [tile setText:@"2"];
            break;
        }
    }
}

-(IBAction)moveRight:(id)sender
{
    NSMutableArray *tileIntArray0 = [NSMutableArray array];
    int itr0 = 0;
    
    for(UILabel *tile in self.row0B){
        if([self isEmpty:tile]){
            itr0 = 10;
            [tileIntArray0 addObject:[NSNumber numberWithInteger:itr0]];
        }else{
            itr0 = tile.text.intValue;
            [tileIntArray0 addObject:[NSNumber numberWithInteger:itr0]];
        }
        NSLog(@"Int value: %d", itr0);
    }
    NSLog(@"%@", tileIntArray0);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray0[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray0[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray0[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray0[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray0[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray0[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray0[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray0[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray0[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray0 objectAtIndex:i] integerValue] == [[tileIntArray0 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray0 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray0[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray0[j] = @"10";
            }
            if(tileIntArray0[3] == NULL){
                tileIntArray0[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray0);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray0 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray0[j] = tileIntArray0[j+1];
            }
            
            [tileIntArray0 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray0);
    
    int count0 = 0;
    
    for(UILabel *tile in self.row0B){
        NSLog(@"Tile %d = %@", count0, tile.text);
        NSLog(@"tIA index %d = %@", count0, [tileIntArray0 objectAtIndex:count0]);
        
        if([[tileIntArray0 objectAtIndex:count0] integerValue] == 2){
            NSLog(@"TILE %d == 2", count0);
            [tile setText:@"2"];
        }
        else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 4){
            NSLog(@"TILE %d == 4", count0);
            [tile setText:@"4"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 8){
            NSLog(@"TILE %d == 8", count0);
            [tile setText:@"8"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 16){
            NSLog(@"TILE %d == 16", count0);
            [tile setText:@"16"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 32){
            NSLog(@"TILE %d == 32", count0);
            [tile setText:@"32"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 64){
            NSLog(@"TILE %d == 64", count0);
            [tile setText:@"64"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 128){
            NSLog(@"TILE %d == 128", count0);
            [tile setText:@"128"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 256){
            NSLog(@"TILE %d == 256", count0);
            [tile setText:@"256"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 512){
            NSLog(@"TILE %d == 512", count0);
            [tile setText:@"512"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count0);
            [tile setText:@"1024"];
        }else if([[tileIntArray0 objectAtIndex:count0] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count0);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count0++;
        
        
    }
    
    
    NSMutableArray *tileIntArray1 = [NSMutableArray array];
    int itr1 = 0;
    
    for(UILabel *tile in self.row1B){
        if([self isEmpty:tile]){
            itr1 = 10;
            [tileIntArray1 addObject:[NSNumber numberWithInteger:itr1]];
        }else{
            itr1 = tile.text.intValue;
            [tileIntArray1 addObject:[NSNumber numberWithInteger:itr1]];
        }
        NSLog(@"Int value: %d", itr1);
    }
    NSLog(@"%@", tileIntArray1);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray1[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
        }else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray1[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray1[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray1[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray1[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray1[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray1[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray1[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray1[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray1 objectAtIndex:i] integerValue] == [[tileIntArray1 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray1 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray1[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray1[j] = @"10";
            }
            if(tileIntArray1[3] == NULL){
                tileIntArray1[3] = @"10";
            }
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray1);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray1 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray1[j] = tileIntArray1[j+1];
            }
            
            [tileIntArray1 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray1);
    
    int count1 = 0;
    
    for(UILabel *tile in self.row1B){
        NSLog(@"Tile %d = %@", count1, tile.text);
        NSLog(@"tIA index %d = %@", count1, [tileIntArray1 objectAtIndex:count1]);
        
        if([[tileIntArray1 objectAtIndex:count1] integerValue] == 2){
            NSLog(@"TILE %d == 2", count1);
            [tile setText:@"2"];
        }
        else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 4){
            NSLog(@"TILE %d == 4", count1);
            [tile setText:@"4"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 8){
            NSLog(@"TILE %d == 8", count1);
            [tile setText:@"8"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 16){
            NSLog(@"TILE %d == 16", count1);
            [tile setText:@"16"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 32){
            NSLog(@"TILE %d == 32", count1);
            [tile setText:@"32"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 64){
            NSLog(@"TILE %d == 64", count1);
            [tile setText:@"64"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 128){
            NSLog(@"TILE %d == 128", count1);
            [tile setText:@"128"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 256){
            NSLog(@"TILE %d == 256", count1);
            [tile setText:@"256"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 512){
            NSLog(@"TILE %d == 512", count1);
            [tile setText:@"512"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count1);
            [tile setText:@"1024"];
        }else if([[tileIntArray1 objectAtIndex:count1] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count1);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count1++;
        
        
    }
    
    NSMutableArray *tileIntArray2 = [NSMutableArray array];
    int itr2 = 0;
    
    for(UILabel *tile in self.row2B){
        if([self isEmpty:tile]){
            itr2 = 10;
            [tileIntArray2 addObject:[NSNumber numberWithInteger:itr2]];
        }else{
            itr2 = tile.text.intValue;
            [tileIntArray2 addObject:[NSNumber numberWithInteger:itr2]];
        }
        NSLog(@"Int value: %d", itr2);
    }
    NSLog(@"%@", tileIntArray2);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray2[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
        }else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray2[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray2[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray2[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray2[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray2[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray2[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray2[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray2[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        } else if(([[tileIntArray2 objectAtIndex:i] integerValue] == [[tileIntArray2 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray2 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray2[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray2[j] = @"10";
            }
            if(tileIntArray2[3] == NULL){
                tileIntArray2[3] = @"10";
            }
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray2);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray2 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray2[j] = tileIntArray2[j+1];
            }
            
            [tileIntArray2 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray2);
    
    int count2 = 0;
    
    for(UILabel *tile in self.row2B){
        NSLog(@"Tile %d = %@", count2, tile.text);
        NSLog(@"tIA index %d = %@", count2, [tileIntArray2 objectAtIndex:count2]);
        
        if([[tileIntArray2 objectAtIndex:count2] integerValue] == 2){
            NSLog(@"TILE %d == 2", count2);
            [tile setText:@"2"];
        }
        else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 4){
            NSLog(@"TILE %d == 4", count2);
            [tile setText:@"4"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 8){
            NSLog(@"TILE %d == 8", count2);
            [tile setText:@"8"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 16){
            NSLog(@"TILE %d == 16", count2);
            [tile setText:@"16"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 32){
            NSLog(@"TILE %d == 32", count2);
            [tile setText:@"32"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 64){
            NSLog(@"TILE %d == 64", count2);
            [tile setText:@"64"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 128){
            NSLog(@"TILE %d == 128", count2);
            [tile setText:@"128"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 256){
            NSLog(@"TILE %d == 256", count2);
            [tile setText:@"256"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 512){
            NSLog(@"TILE %d == 512", count2);
            [tile setText:@"512"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count2);
            [tile setText:@"1024"];
        }else if([[tileIntArray2 objectAtIndex:count2] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count2);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count2++;
        
        
    }
    
    
    NSMutableArray *tileIntArray3 = [NSMutableArray array];
    int itr3 = 0;
    
    for(UILabel *tile in self.row3B){
        if([self isEmpty:tile]){
            itr3 = 10;
            [tileIntArray3 addObject:[NSNumber numberWithInteger:itr3]];
        }else{
            itr3 = tile.text.intValue;
            [tileIntArray3 addObject:[NSNumber numberWithInteger:itr3]];
        }
        NSLog(@"Int value: %d", itr3);
    }
    NSLog(@"%@", tileIntArray3);
    
    for(int i = 0; i < 3; i++){
        if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 2))
        {
            tileIntArray3[i] = @"4";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
        }else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 4))
        {
            tileIntArray3[i] = @"8";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        }else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 8))
        {
            tileIntArray3[i] = @"16";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 16))
        {
            tileIntArray3[i] = @"32";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 32))
        {
            tileIntArray3[i] = @"64";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 64))
        {
            tileIntArray3[i] = @"128";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 128))
        {
            tileIntArray3[i] = @"256";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 256))
        {
            tileIntArray3[i] = @"512";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 512))
        {
            tileIntArray3[i] = @"1024";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        } else if(([[tileIntArray3 objectAtIndex:i] integerValue] == [[tileIntArray3 objectAtIndex:(i+1)] integerValue]) && ([[tileIntArray3 objectAtIndex:i] integerValue] == 1024))
        {
            tileIntArray3[i] = @"2048";
            //NSLog(@"tIA: %@", tileIntArray[i]);
            for(int j = i+1; j < 3; j++){
                tileIntArray3[j] = @"10";
            }
            if(tileIntArray3[3] == NULL){
                tileIntArray3[3] = @"10";
            }
            
            //NSLog(@"Nah");
        }
        
        NSLog(@"%@", tileIntArray3);
        
    }
    
    for(int i = 0; i < 4; i++){
        //Case where tileIntArray[i] == 0, except we will use 10 instead of 0 to avoid the "must be non-null" value issue
        if([[tileIntArray3 objectAtIndex:i] integerValue] == 10){
            for(int j = i; j < 3; j++){
                tileIntArray3[j] = tileIntArray3[j+1];
            }
            
            [tileIntArray3 replaceObjectAtIndex:3 withObject:@"10"];
        }
    }
    NSLog(@"%@", tileIntArray3);
    
    int count3 = 0;
    
    for(UILabel *tile in self.row3B){
        NSLog(@"Tile %d = %@", count3, tile.text);
        NSLog(@"tIA index %d = %@", count3, [tileIntArray3 objectAtIndex:count3]);
        
        if([[tileIntArray3 objectAtIndex:count3] integerValue] == 2){
            NSLog(@"TILE %d == 2", count3);
            [tile setText:@"2"];
        }
        else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 4){
            NSLog(@"TILE %d == 4", count3);
            [tile setText:@"4"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 8){
            NSLog(@"TILE %d == 8", count3);
            [tile setText:@"8"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 16){
            NSLog(@"TILE %d == 16", count3);
            [tile setText:@"16"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 32){
            NSLog(@"TILE %d == 32", count3);
            [tile setText:@"32"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 64){
            NSLog(@"TILE %d == 64", count3);
            [tile setText:@"64"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 128){
            NSLog(@"TILE %d == 128", count3);
            [tile setText:@"128"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 256){
            NSLog(@"TILE %d == 256", count3);
            [tile setText:@"256"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 512){
            NSLog(@"TILE %d == 512", count3);
            [tile setText:@"512"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 1024){
            NSLog(@"TILE %d == 1024", count3);
            [tile setText:@"1024"];
        }else if([[tileIntArray3 objectAtIndex:count3] integerValue] == 2048){
            NSLog(@"TILE %d == 2048", count3);
            [tile setText:@"2048"];
        }else{
            [tile setText:@""];
        }
        
        count3++;
        
        
    }
    
    //Assigns the next empty tile with a value of 2 to keep the game going.
    for(UILabel *tile in self.arrayofTiles){
        if([self isEmpty:tile]){
            [tile setText:@"2"];
            break;
        }
    }

}

-(BOOL) isEmpty:(UILabel *)tileValue{
    if( tileValue.text.length == 0)
        return true;
    else
        return false;
}

-(void)assignRandomTwoToEmptyTile{
    for(UILabel *tile in self.arrayofTiles){
        if([self isEmpty:tile]){
            [tile setText:@"2"];
        }else{
            NSLog(@"GAME OVER");
        }
    }
}


-(BOOL) isEqual:(id)object
{
    
    return true;
}


@end
